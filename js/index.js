$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover(); 
    $(function(){
        $('.carru').carousel({
            interval: 500
        });
    });  
    $('#exampleModal').on('show.bs.modal', function(e){
        console.log("Mostrando");                        
    });
    $('#exampleModal').on('shown.bs.modal', function(e){
        console.log("Mostrado");
        $('#modal_contact').removeClass('btn-outline-success');
        $('#modal_contact').addClass('btn-outline-danger');
        $('#modal_contact').prop('disabled', true);
    });
    $('#exampleModal').on('hide.bs.modal', function(e){
        console.log("Cerrando");
    });
    $('#exampleModal').on('hidden.bs.modal', function(e){
        console.log("Cerrado");
        $('#modal_contact').prop('disabled', false);
        $('#modal_contact').removeClass('btn-outline-danger');
        $('#modal_contact').addClass('btn-outline-success');
    });
})